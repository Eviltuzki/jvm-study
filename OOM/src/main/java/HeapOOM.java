import java.util.ArrayList;
import java.util.List;

public class HeapOOM {

    static class OOMObj{}
    public static void main(String[] args) {
        /**
         * java.lang.OutOfMemoryError: Java heap space
         Dumping heap to java_pid7052.hprof ...
         Heap dump file created [27796707 bytes in 0.142 secs]
         Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
         at java.util.Arrays.copyOf(Arrays.java:3210)
         at java.util.Arrays.copyOf(Arrays.java:3181)
         at java.util.ArrayList.grow(ArrayList.java:265)
         at java.util.ArrayList.ensureExplicitCapacity(ArrayList.java:239)
         at java.util.ArrayList.ensureCapacityInternal(ArrayList.java:231)
         at java.util.ArrayList.add(ArrayList.java:462)
         at HeapOOM.main(HeapOOM.java:10)
         */

        List<OOMObj> oomObjs = new ArrayList<>();
        while (true)
            oomObjs.add(new OOMObj());
    }
}
