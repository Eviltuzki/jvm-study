public class JVMOOM {
    private void notStop(){
        while (true){}
    }


    public void stackLeakByThread(){
        while (true){
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                   notStop();
                }
            });
            thread.start();
        }
    }

    public static void main(String[] args) {
        new JVMOOM().stackLeakByThread();
    }
}
