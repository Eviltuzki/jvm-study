import java.util.ArrayList;
import java.util.List;

public class RuntimeConstantPoolOOM {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        int i = 0;
        while (true){
            strings.add(String.valueOf(i++).intern());
        }
    }
}
